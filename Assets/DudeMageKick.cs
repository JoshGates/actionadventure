﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DudeMageKick : MonoBehaviour {

    public int damage = 2;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            HitObject(other.gameObject);
            DamageCooldown.cooldown = true;
        }
    }

    void HitObject(GameObject g)
    {
        HealthController health = g.GetComponent<HealthController>();
        if (health != null)
        {
            health.TakeDamage(damage);
        }
    }
}
