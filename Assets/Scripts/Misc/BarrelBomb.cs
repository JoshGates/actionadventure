﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelBomb : MonoBehaviour {

    public static bool timer = false;
    ParticleSystem part;

    private void Awake()
    {
        part = GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if (timer == true)
        {
            StartCoroutine(Explode());
        }
    }

    IEnumerator Explode()
    {
        yield return new WaitForSeconds(4);
        part.Play();
        yield return new WaitForSeconds(6);
        this.gameObject.SetActive(false);
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.CompareTag("Enemy-Ragdoll")) { 
            Rigidbody[] otherRagdolls = other.transform.Find("NinjaDudeRagdoll").GetComponentsInChildren<Rigidbody>();
            if (other.CompareTag("Enemy-RagdollN") && RelationsTracker.NeutralNinjasAngry == false)
            {
                RelationsTracker.NeutralNinjasAngry = true;
            }
            for (int i = 0; i < otherRagdolls.Length; i++)
            {
                Rigidbody otherRagdoll = otherRagdolls[i];
                otherRagdoll.gameObject.SetActive(true);
                otherRagdoll.transform.SetParent(null);
                other.gameObject.SetActive(false);
                otherRagdoll.AddForce(transform.forward * 800);
            }
        }
    }
}