﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TravelScript : MonoBehaviour {


    void OnTriggerEnter(Collider other)
    {
        if (SceneManager.GetActiveScene().name == "Overworld" && other.CompareTag("Player"))
        {
            SceneManager.LoadScene("Cave");
        }
        else if (SceneManager.GetActiveScene().name == "Cave" && other.CompareTag("Player"))
        {
            SceneManager.LoadScene("Main");
        }    
    }
}
