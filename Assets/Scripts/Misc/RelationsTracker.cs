﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelationsTracker : MonoBehaviour {

    public RelationsTracker instance;

    public static bool NeutralNinjasAngry;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        NeutralNinjasAngry = false;
    }

    void NinjasMad()
    {
        NeutralNinjasAngry = true;
    }

}
