﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignReadScript : MonoBehaviour {

    public SpriteRenderer prompt;

    public string[] messages;

	void Update () {
		if (Vector3.Distance(this.transform.position, PlayerControllerPhysics.instance.transform.position) < 2)
        {
            prompt.gameObject.SetActive(true);
            UIScript.instance.messages = messages;
        }
        else
        {
            prompt.gameObject.SetActive(false);
        }
        if (Vector3.Distance(this.transform.position, PlayerControllerPhysics.instance.transform.position) < 2 && Input.GetButtonDown("Fire1") && !UIScript.instance.main.gameObject.activeSelf)
        {
            UIScript.SignText();
        }

    }
}
