﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuManager : MonoBehaviour {

    public RectTransform pauseMenuRoot;

    void Awake()
    {
        ResumeGame();
    }

    public void PauseGame()
    {
        pauseMenuRoot.gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        pauseMenuRoot.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

	public void QuitGame()
    { 
        Application.Quit();
    }

    public void SaveGame()
    {
        SaveManager.Save();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (pauseMenuRoot.gameObject.activeSelf)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }



}
