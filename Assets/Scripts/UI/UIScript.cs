﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIScript : MonoBehaviour {

    public static UIScript instance;

    public string[] messages;

    public bool nextMsg = false;
    public Image background;
    public Image face1;
    public Image face2;
    public Image face3;
    public Image face4;
    public Image face5;
    public Image face6;

    public Text main;

    public RectTransform pauseMenuRoot;
    public HealthController playerHealthController;
    

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
        playerHealthController = PlayerControllerPhysics.instance.GetComponent<HealthController>();
    }


    void Update()
    {
        if (playerHealthController.healthPercent > 80)
        {
            face1.gameObject.SetActive(true);
        }
        else if (playerHealthController.healthPercent > 60)
        {
            face2.gameObject.SetActive(true);
        }
        else if(playerHealthController.healthPercent > 40)
        {
            face3.gameObject.SetActive(true);
        }
        else if (playerHealthController.healthPercent > 20)
        {
            face4.gameObject.SetActive(true);
        }
        else if (playerHealthController.healthPercent > 0)
        {
            face5.gameObject.SetActive(true);
        }
        else
        {
            face6.gameObject.SetActive(true);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            nextMsg = true;
        }
    }

    public bool DumbFunc()
    {
        return nextMsg;
    }

    public static void SignText()
    {
        instance.StartCoroutine(instance.SignTextCoroutine());
    }

    public IEnumerator SignTextCoroutine()
    {
        nextMsg = false;
        main.gameObject.SetActive(true);
        background.gameObject.SetActive(true);
        
        for (int i = 0; i < messages.Length; i++)
        {
            main.text = messages[i];
            yield return new WaitUntil(DumbFunc);
            nextMsg = false;
        }
        main.gameObject.SetActive(false);
        background.gameObject.SetActive(false);
    }
}
