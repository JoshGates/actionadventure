﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameEntryController : MonoBehaviour {

	public int fileID = 0;
	public string allLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ-.,!'&0123456789 ";

	public static NameEntryController instance;

	public LetterButtonController letterButtonPrefab;
	public RectTransform lettersRoot;

	public InputField inputField;

	void Awake () {
		if (instance == null) instance = this;
		SetupButtons();
	}

	void OnEnable() {
		StartCoroutine(Reset());        
	}

	IEnumerator Reset () {
		yield return new WaitForEndOfFrame();
		inputField.text = "";
		lettersRoot.GetChild(0).GetComponent<Selectable>().Select();
	}

	public static void AddLetter (string letter) {
		instance.inputField.text += letter;
	}

	public void SetupButtons () {
		for (int i = lettersRoot.childCount-1; i >= 0; i--) {
			Destroy(lettersRoot.GetChild(i).gameObject);
		}

		for (int i = 0; i < allLetters.Length; i++) {
			LetterButtonController b = Instantiate(letterButtonPrefab) as LetterButtonController;
			b.transform.SetParent(lettersRoot);
			b.transform.localScale = Vector3.one;
			b.SetupWithLetter(allLetters[i].ToString());
		}
    }

    public void CancelButtonClicked() {
		MainMenuManager.instance.ShowFileSelectScreen();
    }

    public void SubmitButtonClicked() {
		SaveManager.Create(fileID, inputField.text);
		MainMenuManager.instance.ShowFileSelectScreen();
    }

    public void ClearButtonClicked() {
		inputField.text = "";
    }

    public void DeleteButtonClicked() {
		string s = inputField.text;
        inputField.text = s.Substring(0, s.Length - 1);
    }
}
