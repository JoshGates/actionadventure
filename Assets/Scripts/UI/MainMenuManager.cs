﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour {

    public static MainMenuManager instance;

    public RectTransform startScreen;
    public RectTransform fileSelectScreen;
    public RectTransform newGameScreen;

    public Selectable startScreenSelectable;
    public Selectable fileSelectSelectable;

    public GameObject deleteFilePrompt;

    public Screen currentScreen = Screen.Start;

    private FileSelectController fileSelect;
    
    public enum Screen
    {
        Start,
        FileSelect,
        NewGame
    }
        
    void Awake () {
        if (instance == null) instance = this;
        ShowStartScreen();
        deleteFilePrompt.SetActive(false);
    }

    void Update () {
        if (Input.GetButtonDown("Cancel")) {
            switch (currentScreen) {
                case Screen.FileSelect:
                    ShowStartScreen();
                    break;
                case Screen.NewGame:
                    ShowFileSelectScreen();
                    break;
            }
        }
    }
        
    public void ShowStartScreen()
    {
        currentScreen = Screen.Start;
        startScreen.gameObject.SetActive(true);
        fileSelectScreen.gameObject.SetActive(false);
        newGameScreen.gameObject.SetActive(false);
        startScreenSelectable.Select();
    }

    public void ShowFileSelectScreen() {
        currentScreen = Screen.FileSelect;
        startScreen.gameObject.SetActive(false);
        fileSelectScreen.gameObject.SetActive(true);
        newGameScreen.gameObject.SetActive(false);
        fileSelectSelectable.Select();
    }

    public void ShowNewGameScreen() {
        currentScreen = Screen.NewGame;
        startScreen.gameObject.SetActive(false);
        fileSelectScreen.gameObject.SetActive(false);
        newGameScreen.gameObject.SetActive(true);
    }
       
    public void PromptDelete(FileSelectController fileSelect)
    {
        this.fileSelect = fileSelect;
        deleteFilePrompt.SetActive(true);
    }

    public void ConfirmDelete (bool confirm)
    {
        deleteFilePrompt.SetActive(false);
        if (confirm)
        {
            fileSelect.ConfirmDelete();
        }
    }
}
