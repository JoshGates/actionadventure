﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterButtonController : MonoBehaviour {

	public string letter;

	public Text displayLetter;

	public void SetupWithLetter (string letter) {
		this.letter = letter;
		displayLetter.text = letter;
	}

	public void ButtonClicked () {
		NameEntryController.AddLetter(letter);
	}
}
