﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushScript : MonoBehaviour {

    Rigidbody rb;
    private bool trapOn;
    private bool cooldown;
    public ParticleSystem part;

    private void Start()
    {
        trapOn = false;
        cooldown = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!cooldown)
        {
            trapOn = true;
            StartCoroutine(PushCooldown());
            part.Play();
        }
    }

    IEnumerator PushCooldown()
    {
        Debug.Log("Active");
        yield return new WaitForSeconds(1);
        trapOn = false;
        cooldown = true;
        yield return new WaitForSeconds(.5f);
        cooldown = false;
    }

    void OnTriggerStay(Collider other)
    {
        Debug.Log("other");
        if (other.GetComponent<Rigidbody>() != null && trapOn)
        {
            rb = other.gameObject.GetComponent<Rigidbody>();
            rb.AddForce(-this.transform.forward * 20000);
        }
    }
}
