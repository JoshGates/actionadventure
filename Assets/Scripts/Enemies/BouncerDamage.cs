﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncerDamage : MonoBehaviour {

    public float damage = 2;

    /*void OnCollisionEnter(Collision c)
    {
        HitObject(c.gameObject);
    }*/

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && DamageCooldown.cooldown != true && PlayerControllerPhysics.IsPlaying)
        {
            HitObject(other.gameObject);
            DamageCooldown.cooldown = true;
        }
    }

    void HitObject(GameObject g)
    {
        HealthController health = g.GetComponent<HealthController>();
        if (health != null)
        {
            health.TakeDamage(damage);
        }
    }
}
