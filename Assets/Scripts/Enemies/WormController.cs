﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormController : MonoBehaviour {

    public float damage = 2;
    public Animator anim;
    private int randomNum;
    private float timeStamp;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        timeStamp = Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        HealthController health = collision.gameObject.GetComponent<HealthController>();
        if (health != null && !collision.gameObject.CompareTag("Player"))
        {
            health.TakeDamage(damage);
        }
    }

    private void Update()
    {
        
        if (randomNum <6)
        {
            anim.SetTrigger("attack");
        }
        if (Time.deltaTime >= timeStamp + 1)
        {
            randomNum = Random.Range(1, 30);
            timeStamp = Time.deltaTime;
        }
        
    }
}