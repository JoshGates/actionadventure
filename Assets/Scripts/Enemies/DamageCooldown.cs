﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageCooldown : MonoBehaviour {

    public static bool cooldown = false;
    private bool working = false;

    void Update()
    {
        if (cooldown != false && !working)
        {
            StartCoroutine(Cooldown());
        }
    }

    IEnumerator Cooldown()
    {
        working = true;
        yield return new WaitForSeconds(.9f);
        cooldown = false;
        working = false;
    }

}
