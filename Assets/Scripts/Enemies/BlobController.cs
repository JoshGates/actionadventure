﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobController : MonoBehaviour {

    public Rigidbody rb;
    public Transform player;
    public float ChaseDist = 100;
    public float speed = 10;
    public float AttackDist = 1;
    public float TurnSpeed = 100;
    public Animator anim;

    public enum State
    {
        Idle,
        Chase,
        Attack,
        Death
    }

    public State state = State.Idle;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update ()
    {	
        switch (state)
        {
            case State.Idle:
                IdleUpdate();
                //Debug.Log("Idle");
                break;
            case State.Chase:
                ChaseUpdate();
                break;
            case State.Attack:
                break;
            case State.Death:
                DeathUpdate();
                break;
            default:
                Debug.Log("Default");
                break;
        }
	}

    public void IdleUpdate()
    {
        rb.velocity = Vector3.zero;
        float dist = Vector3.Distance(transform.position, player.position);
        if (dist < ChaseDist)
        {
            state = State.Chase;
            //Debug.Log("CHASE");
        }
    }
    public void ChaseUpdate()
    {
        Vector3 dir = (player.position - transform.position).normalized;
        Vector3 cross = Vector3.Cross(-transform.right, dir);
        transform.Rotate(Vector3.up * cross.y * TurnSpeed * Time.deltaTime);

        float dist = Vector3.Distance(transform.position, player.position);
        if (dist > ChaseDist)
        {
            state = State.Idle;
            //Debug.Log("IDLE");
            anim.SetTrigger("Idle");
        }
        else if (dist < AttackDist)
        {
            state = State.Attack;
            rb.velocity = Vector3.zero;
            //Debug.Log("ATTACK");
            anim.SetTrigger("Attack");
        }
        else
        {
            rb.velocity = dir * speed;
        }
    }

    public void AttackFinished()
    {

        state = State.Idle;
        anim.SetTrigger("Idle");
    }
    public void DeathUpdate()
    {
        rb.velocity = new Vector3(0,0,0);
        anim.SetTrigger("Dead");
    }
}
