﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public float ChaseDist = 10;
    public float speed = 10;
    public float AttackDist = 1;
    public float ChaseAttackDist = 4;
    public Animator anim;
    private int AttackType;
    private int RollKick;
    private int LastAttack;

    public enum State
    {
        Idle,
        Chase,
        Attack
    }
    public State state = State.Idle;

    Transform player;
    Rigidbody rb;
	void Start () {
        rb = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

	}
	
	void Update () {
		switch (state)
        {
            case State.Idle:
                IdleUpdate();
                break;

            case State.Chase:
                ChaseUpdate();
                break;

            case State.Attack:
                AttackUpdate();
                break;

            default:
                break;
        }
	}
    void IdleUpdate()
    {
        anim.SetInteger("RandomAttack", 0);
        anim.SetBool("Running", false);
        rb.velocity = Vector3.zero;
        float dist = Vector3.Distance(transform.position, player.position);
        if (dist < ChaseDist)
        {
            state = State.Chase;
            //Debug.Log("CHASE");
        }
    }
    void ChaseUpdate()
    {
        anim.SetInteger("RandomAttack", 0);
        float dist = Vector3.Distance(transform.position, player.position);
        if (dist > ChaseDist)
        {
            state = State.Idle;
            //Debug.Log("IDLE");
        }
        else if(dist < AttackDist)
        {
            anim.SetTrigger("Fighting");
            state = State.Attack;
            attackStart = Time.time;
            //Debug.Log("ATTACK");
        }
        else
        {
            anim.SetBool("Running", true);
            Vector3 dir = (player.transform.position - this.transform.position).normalized;
            this.transform.LookAt(player);
            rb.velocity = dir * speed;
        }
    }
    float attackStart;
    void AttackUpdate()
    {
        AttackType = Random.Range(1, 5);
        anim.SetInteger("ATTACKTYPE", (int)AttackType);
        anim.SetBool("Running", false);
        Vector3 dir = (player.transform.position - this.transform.position).normalized;
        rb.velocity = -dir * speed * 0.5f;
        this.transform.LookAt(player);
        if (AttackType == 1 && LastAttack!=1)
        {
            anim.SetInteger("RandomAttack", 1);
            LastAttack = 1;
            //Debug.Log(AttackType);
        }
        else if (AttackType == 2 && LastAttack != 2)
        {
            anim.SetInteger("RandomAttack", 2);
            LastAttack = 2;
            //Debug.Log(AttackType);
        }
        else if (AttackType == 3 && LastAttack != 3)
        {
            anim.SetInteger("RandomAttack", 3);
            LastAttack = 3;
            //Debug.Log(AttackType);
        }
        else if (AttackType == 4 && LastAttack != 4)
        {
            anim.SetInteger("RandomAttack", 4);
            LastAttack = 4;
            //Debug.Log(AttackType);
        }
        if (Time.time - attackStart > 1)
        {
            state = State.Idle;
            //Debug.Log("IDLE");
        }
    }
}