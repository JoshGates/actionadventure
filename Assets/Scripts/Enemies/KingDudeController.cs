﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingDudeController : MonoBehaviour {

    public float ChaseDist = 100;
    public float speed = 10;
    public float chaseSpeed = 50;
    public float AttackDist = 1;
    public float ChaseAttackDist = 4;
    private int attackType = 0;
    private int lastAttack = 0;
    
    private bool moveTowardsPlayer;

    public GameObject center;
    public ParticleSystem waterStream;
    public ParticleSystem areaAttack;
    Animator anim;

    public enum State
    {
        Nothing,
        Idle,
        Attack,
        Recovering,
        Attacking,
        Dead
    }
    public State state = State.Nothing;

    Transform player;
    Rigidbody rb;
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        Debug.Log(state);
    }

    void Update()
    {
     
        if (Vector3.Distance(transform.position, PlayerControllerPhysics.instance.transform.position) < AttackDist)
        {
            state = State.Idle;
        }

        switch (state)
        {
            case State.Nothing:
                break;

            case State.Attacking:
                break;

            case State.Idle:
                this.transform.LookAt(player);
                IdleUpdate();
                //Debug.Log(state);
                break;

            /*case State.Chase:
                ChaseUpdate();
                break;*/

            case State.Attack:
                this.transform.LookAt(player);
                AttackUpdate();
                //Debug.Log(state);
                break;

            case State.Recovering:
                StartCoroutine(RecoveringUpdate());
                break;

            default:
                break;
        }
    }
    void IdleUpdate()
    {
        transform.LookAt(PlayerControllerPhysics.instance.transform.position + Vector3.up);
        float dist = Vector3.Distance(transform.position, player.position);
        state = State.Attack;
        //Debug.Log(state);
    }
    /*void ChaseUpdate()
    {
        float dist = Vector3.Distance(transform.position, player.position);
        if (dist > ChaseDist)
        {
            state = State.Idle;
            //Debug.Log("IDLE");
        }
        else if (dist < AttackDist)
        {
            anim.SetTrigger("Fighting");
            state = State.Attack;
            attackStart = Time.time;
            //Debug.Log("ATTACK");
        }
        else
        {
            Vector3 dir = (player.transform.position - this.transform.position).normalized;
            this.transform.LookAt(player);
            rb.velocity = dir * speed;
        }
    }*/

    void AttackUpdate()
    {

        while (attackType == lastAttack)
        {
            attackType = Random.Range(1, 4);
        }
        
        Vector3 dir = (player.transform.position - this.transform.position).normalized;
        rb.velocity = -dir * speed * 0.5f;
        
        anim.SetInteger("AttackType", attackType);
        state = State.Attacking;

        if (attackType == 1)
        {
            StartCoroutine(WaterAttack());
        }
        else if (attackType == 2)
        {
            StartCoroutine(AreaAttack());
        }
        else if (attackType == 3)
        {
            StartCoroutine(HurricaneKick());
        }
        lastAttack = attackType;
        //Debug.Log(attackType);
    }

    IEnumerator WaterAttack()
    {
        rb.velocity = Vector3.zero;
        int r = Random.Range(2, 5);
        for (int i = 0; i < r; i++)
        {
            this.transform.LookAt(PlayerControllerPhysics.instance.transform);
            this.gameObject.transform.LookAt(PlayerControllerPhysics.instance.gameObject.transform);
            waterStream.Play();
            yield return new WaitForSeconds(1.5f);
        }
        anim.SetTrigger("MagicAttackOver");
        anim.SetInteger("AttackType", 0);
        state = State.Idle;
    }

    IEnumerator AreaAttack()
    {
        rb.velocity = Vector3.zero;
        for (float i = 0; i < 4; i += Time.deltaTime)
        {
            float frac = i / 4;
            transform.position = Vector3.Lerp(transform.position, center.transform.position, frac);
            yield return new WaitForEndOfFrame();
        }
        anim.Play("AreaAttack");
        yield return new WaitForSeconds(1.22f);
        areaAttack.Play();
        anim.SetInteger("AttackType", 0);
        yield return new WaitForSeconds(5);
        anim.SetInteger("AttackType", 0);
        state = State.Recovering;
    }

    IEnumerator HurricaneKick()
    {
        //Debug.Log("here");
        moveTowardsPlayer = true;
        MoveTowardsPlayer();
        yield return new WaitForSeconds(4);
        anim.SetTrigger("KickEnd");
        anim.SetInteger("AttackType", 0);
        moveTowardsPlayer = false;
        state = State.Idle;
    }

    IEnumerator RecoveringUpdate()
    {
        state = State.Attacking;
        anim.SetTrigger("Cooldown");
        yield return new WaitForSeconds(4);
        anim.SetTrigger("CooldownOver");
        yield return new WaitForSeconds(1);
        state = State.Idle;
    }

    IEnumerator MoveTowardsPlayer()
    {
        while (moveTowardsPlayer)
        {
            Vector3 diff = PlayerControllerPhysics.instance.transform.position - transform.position;
            rb.velocity = diff.normalized * chaseSpeed;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForEndOfFrame();
    }
}