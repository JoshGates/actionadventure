﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaAttack : MonoBehaviour {

    public ParticleSystem part;
    private int damage = 2;

    void Start()
    {
        part = GetComponent<ParticleSystem>();
    }

    void OnParticleCollision(GameObject other)
    {
        HealthController health = other.GetComponent<HealthController>();
        if (health != null && other.CompareTag("Player"))
        {
            health.TakeDamage(damage);
        }
    }
}
