﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeutralNinjas : MonoBehaviour
{
    public float ChaseDist = 10;
    public float speed = 10;
    public float attackDist = 1;
    public float ChaseAttackDist = 4;
    public float LookDist = 3;
    public Animator anim;
    private int AttackType;
    private int RollKick;
    private int LastAttack;
    
    public State state = State.Idle;

    Transform player;
    Rigidbody rb;

    float attackStartTimeStamp;

    public enum State
    {
        Idle,
        Chase,
        Attack,
        Neutral
    }
       
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        float dist = Vector3.Distance(transform.position, player.position);
        if (dist < LookDist && !RelationsTracker.NeutralNinjasAngry)
        {
            transform.LookAt(player);
        }
        switch (state)
        {
            case State.Neutral:
                NeutralUpdate();
                break;

            case State.Idle:
                IdleUpdate();
                break;

            case State.Chase:
                ChaseUpdate();
                break;

            case State.Attack:
                AttackUpdate();
                break;

            default:
                break;
        }
    }

    void NeutralUpdate()
    {
        rb.velocity = Vector3.zero;
        if (RelationsTracker.NeutralNinjasAngry)
        {
            state = State.Idle;
            //Debug.Log("Idle");
        }
    }

    void IdleUpdate()
    {
        anim.SetInteger("RandomAttack", 0);
        anim.SetBool("Running", false);
        rb.velocity = Vector3.zero;
        float dist = Vector3.Distance(transform.position, player.position);
        if (dist < ChaseDist)
        {
            state = State.Chase;
            //Debug.Log("CHASE");
        }
    }

    void ChaseUpdate()
    {
        anim.SetInteger("RandomAttack", 0);
        float dist = Vector3.Distance(transform.position, player.position);
        if (dist > ChaseDist)
        {
            state = State.Idle;
            //Debug.Log("IDLE");
        }
        else if (dist < attackDist)
        {
            anim.SetTrigger("Fighting");
            state = State.Attack;
            attackStartTimeStamp = Time.time;
            //Debug.Log("ATTACK");
        }
        else
        {
            anim.SetBool("Running", true);
            Vector3 dir = (player.transform.position - transform.position).normalized;
            transform.LookAt(player);
            rb.velocity = dir * speed;
        }
    }
       
    void AttackUpdate()
    {
        AttackType = Random.Range(1, 5);
        anim.SetInteger("ATTACKTYPE", AttackType);
        anim.SetBool("Running", false);
        Vector3 dir = (player.transform.position - transform.position).normalized;
        rb.velocity = -dir * speed * 0.5f;
        transform.LookAt(player);

        if (AttackType != LastAttack)
        {
            anim.SetInteger("RandomAttack", AttackType);
            LastAttack = AttackType;
            Debug.Log(AttackType);
        }

        if (Time.time - attackStartTimeStamp > 1)
        {
            state = State.Idle;
            //Debug.Log("IDLE");
        }
    }
}