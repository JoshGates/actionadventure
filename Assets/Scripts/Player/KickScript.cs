﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KickScript : MonoBehaviour {

    Rigidbody otherRB;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Kickable"))
        {
            Debug.Log(other);
            otherRB = other.GetComponent<Rigidbody>();
            otherRB.AddForce(transform.forward * 5000);
        }
        else if (other.CompareTag("Enemy-Ragdoll") || other.CompareTag("Enemy-RagdollN"))
        {
            Rigidbody[] otherRagdolls = other.transform.Find("NinjaDudeRagdoll").GetComponentsInChildren<Rigidbody>();
            if (other.CompareTag("Enemy-RagdollN") && RelationsTracker.NeutralNinjasAngry == false)
            {
                RelationsTracker.NeutralNinjasAngry = true;
            }
            for (int i = 0; i < otherRagdolls.Length; i++)
            {
                Rigidbody otherRagdoll = otherRagdolls[i];
                otherRagdoll.gameObject.SetActive(true);
                otherRagdoll.transform.SetParent(null);
                other.gameObject.SetActive(false);
                otherRagdoll.AddForce(transform.forward * 800);
            }
        }
        else if (other.CompareTag("Enemy"))
        {
            other.gameObject.SetActive(false);
            
        }
    }
    
}
