﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerPhysics : MonoBehaviour {

    public static PlayerControllerPhysics instance;

    public float forwardSpeed = 10;
    private float forwardSpeedSelected;
    public float turnSpeed = 5;

    public bool ancientTeachings = false;
    public static bool IsPlaying = true;
    private bool hit = true;
    private bool taunting = false;
    private bool moving = true;
    private bool xLock = false;
    private bool magicCooldown = false;

    public LayerMask layerMask;
    public LayerMask layerMask2;
    public LayerMask layerMask3;

    Vector3 PastPosition;

    public Rigidbody rb;

    public GameObject Panel;
    public GameObject KickTrigger1;
    public GameObject KickTrigger2;
    public GameObject PunchTrigger;

    public ParticleSystem part;

    Animator anim;
    public Animator lever;


    void Awake()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        StartCoroutine(Speed());
        if (instance == null)
        {
            instance = this;
        }
        forwardSpeedSelected = forwardSpeed;
    }

    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        float s = Input.GetAxis("Fire3");

        if (s != 0)
        {
            RaycastHit RHit;
            if (IsPlaying && Physics.Raycast(transform.position * 1.1f, -transform.up, out RHit, 1, layerMask3) == true)
            {
                moving = false;
                anim.SetBool("Sprinting", false);
            }
            else
            {
            moving = true;
            forwardSpeed = forwardSpeedSelected;
            anim.SetBool("Sprinting", true);
            forwardSpeed = forwardSpeed * 3;
            }
            
        }
        else
        {
            anim.SetBool("Sprinting", false);
            forwardSpeed = forwardSpeedSelected;
        }

        if (!IsPlaying)
        {
            x = 0;
            y = 0;
        }

        if (xLock)
        {
            y = 0;
        }

        if (moving)
        {
            Vector3 v = transform.forward * y * forwardSpeed;
            v.y = rb.velocity.y;
            rb.velocity = v;
            rb.angularVelocity = Vector3.up * x * turnSpeed;
        }
        anim.SetFloat("x", rb.angularVelocity.y);
        anim.SetFloat("y", Vector3.Dot(rb.velocity, transform.forward));

        if (IsPlaying && ancientTeachings && Input.GetKeyDown("t"))
        {
            moving = false;
            anim.SetTrigger("Shake");
            taunting = true;
            StartCoroutine(Dancing());
        }

        if (IsPlaying && ancientTeachings && Input.GetKeyDown("r") && !magicCooldown)
        {
            xLock = true;
            StartCoroutine(Magic());
        }

        if (IsPlaying && Input.GetKeyDown("e"))
        {
            RaycastHit interactableHit;
            if (Physics.Raycast(transform.position + Vector3.up, transform.forward, out interactableHit, 2, layerMask))
            {
                Debug.Log("hit" + interactableHit);
                Animator anim = interactableHit.collider.GetComponentInParent<Animator>();
                anim.SetTrigger("OpenDoor");
            }
            if (Physics.Raycast(transform.position + Vector3.up, transform.forward, out interactableHit, 2, layerMask2))
            {
                Debug.Log("hit" + interactableHit);
                Panel.SetActive(false);
                lever.Play("PullLever");
                BarrelBomb.timer = true;
            }
        }
        

        if (Input.GetKeyDown("space") && IsPlaying)
        { 
            if (y >= 1)
            {
                moving = false;
                StartCoroutine(Kick());
            }
            else
            {
                xLock = true;
                StartCoroutine(Punch());
            }
        }
    }

    IEnumerator Dancing()
    {
        yield return new WaitForSeconds(15.22f);
        moving = true;
    }

    IEnumerator Speed()
    {
        while (enabled)
        {

            PastPosition = this.transform.position;

            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator Magic()
    {
        anim.SetTrigger("Magic");
        yield return new WaitForSeconds(.85f);
        part.Play();
        yield return new WaitForSeconds(1.2f);
        magicCooldown = true;
        xLock = false;
        yield return new WaitForSeconds(10);
        magicCooldown = false;
    }

    IEnumerator Kick()
    {
        hit = false;
        anim.SetTrigger("Kick");
        yield return new WaitForSeconds(.1f);
        KickTrigger1.SetActive(true);
        KickTrigger2.SetActive(true);
        rb.velocity = this.transform.forward * 2;
        yield return new WaitForSeconds(.5f);
        moving = true;
        KickTrigger1.SetActive(false);
        KickTrigger2.SetActive(false);
        rb.velocity = Vector3.zero;
        /*while (hit == false)
        {
            RaycastHit RHit;
            if (Physics.Raycast(transform.position, Vector3.down, out RHit, 2))
            {
                hit = true;
            }

        }
        anim.SetTrigger("Landed");*/
    }
    IEnumerator Punch()
    {
        anim.SetTrigger("Punch");
        yield return new WaitForSeconds(.4f);
        PunchTrigger.SetActive(true);
        yield return new WaitForSeconds(.7f);
        xLock = false;
        PunchTrigger.SetActive(false);
    }
    public IEnumerator PlayerHit()
        {
            moving = false;
            anim.SetTrigger("Hit");
            yield return new WaitForSeconds(.25f);
            moving = true;
    }
}