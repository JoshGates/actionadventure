﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float PastHealth;
    public static float CurrentHealth = 30;
    public float forwardSpeed = 10;
    public float turnSpeed = 5;
    public static bool IsPlaying = true;
    private float moving;

    Vector3 PastPosition;

    public Rigidbody rb;

    public GameObject KickTrigger1;
    public GameObject KickTrigger2;
    public GameObject PunchTrigger;

    Animator anim;

    void Awake() 
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        StartCoroutine(Speed());
        PastHealth = CurrentHealth;
    }

    void Update()
    {
        if (IsPlaying == true)
        {
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            anim.SetFloat("x", x * turnSpeed);
            anim.SetFloat("y", y * forwardSpeed);
            moving = y;
        }

        if (PastHealth != CurrentHealth && IsPlaying == true)
        {
            PastHealth = CurrentHealth;
            anim.SetTrigger("Hit");
        }
        if (CurrentHealth <= 0)
        {
            anim.SetTrigger("Dead");
            IsPlaying = false;
        }
        if (Input.GetKeyDown("space") && IsPlaying)
        {
            if (moving >=1 )
            {
                StartCoroutine(Kick());
            }
            else
            {
                StartCoroutine(Punch());
            }      
        }
        
    }
    IEnumerator Speed()
    {
        while (enabled)
        {

            PastPosition = this.transform.position;

            yield return new WaitForEndOfFrame();
        }
    }
    IEnumerator Kick()
    {
        anim.SetTrigger("Kick");
        yield return new WaitForSeconds(.1f);
        KickTrigger1.SetActive(true);
        KickTrigger2.SetActive(true);
        yield return new WaitForSeconds(.5f);
        KickTrigger1.SetActive(false);
        KickTrigger2.SetActive(false);
    }
    IEnumerator Punch()
    {
        anim.SetTrigger("Punch");
        yield return new WaitForSeconds(.4f);
        PunchTrigger.SetActive(true);
        yield return new WaitForSeconds(.5f);
        PunchTrigger.SetActive(false);
    }
}
