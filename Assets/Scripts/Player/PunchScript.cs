﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchScript : MonoBehaviour {

    Rigidbody otherRB;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Kickable"))
        {
            Debug.Log(other);
            otherRB = other.GetComponent<Rigidbody>();
            otherRB.AddForce(transform.forward * 5000);
        }
    }
}
