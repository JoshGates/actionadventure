﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour
{

    Animator anim;
    public float maxMagicDefense = 100;
    public float magicDefense = 5;
    public float maxHealth = 5;
    public float health;
    public float healthPercent;
    private float lastHealth;

    public bool god = false;

    void Awake()
    {
        health = maxHealth;
        anim = GetComponent<Animator>();
        lastHealth = health;
        healthPercent = health / maxHealth * 100;
    }

    void Update()
    {
        if (lastHealth != health)
        {
            lastHealth = health;
            healthPercent = health / maxHealth * 100;
            if (this.GetComponentInParent<PlayerControllerPhysics>() != null)
            {
                StartCoroutine(PlayerControllerPhysics.instance.PlayerHit());
            }
        }
    }

    public void TakeDamage(float damage)
    {
        if (health > 0 && !god)
        {
            health -= damage;
            anim.SetTrigger("Hit");
        }
        else if (health <= 0)
        {
            anim.SetTrigger("Dead");
            PlayerControllerPhysics.IsPlaying = false;
        }
    }
    public void TakeMagicDamage(float magicDamage)
    {
        if (magicDefense > 0 && !god)
        {
            magicDefense -= magicDamage;
            Debug.Log(magicDefense);
        }
        else
        {
            if (this.CompareTag("Enemy-RagdollN") || this.CompareTag("Enemy-Ragdoll"))
            {
                Rigidbody[] otherRagdolls = this.transform.Find("NinjaDudeRagdoll").GetComponentsInChildren<Rigidbody>();
                if (this.CompareTag("Enemy-RagdollN") && RelationsTracker.NeutralNinjasAngry == false)
                {
                    RelationsTracker.NeutralNinjasAngry = true;
                }

                int length = otherRagdolls.Length;

                for (int i = 0; i < length; i++)
                {
                    Rigidbody otherRagdoll = otherRagdolls[i];
                    otherRagdoll.gameObject.SetActive(true);
                    otherRagdoll.transform.SetParent(null);
                    this.gameObject.SetActive(false);
                    otherRagdoll.AddForce(transform.forward * 800);
                }
            }
            else
            {
                this.gameObject.SetActive(false);
            }
        }
    }
}
