﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DudeMageSpawn : MonoBehaviour
{

    public GameObject dudeMage;
    EnemyController[] honorGuard;

    private void Awake()
    {
        honorGuard = FindObjectsOfType<EnemyController>();
        
    }
    
	
	void Update ()
    {
        bool allDead = true;
        for (int i = 0; i < honorGuard.Length; i++)
        {
            if (honorGuard[i].gameObject.activeSelf)
            {
                allDead = false;
                break;
            }
        }
        if (allDead)
        {
            dudeMage.SetActive(true);
        }
    }
}
